# JLDropOff-Front-end
As part of the internship completed from 01/11/2021 to 03/05/2021, the JLDropOff application was created. <br/>
Aiming to comply with the General Data Protection Regulation (GDPR), this application was created so that insurance clients can upload the files required for the finalization of their case. <br/>
The front-end part of JLDropOff was made with Ionic, version 6.

## Services
The JLDropOff project contains the following services:
### affair.service, in which is implemented:
    - The getAffair() method, which contains an id_user parameter, then an API request on the affair url.

### authentication.service, in which are implemented:
    - The registration() method, which contains a user parameter, then an API request on the register url.
    - The login() method, which contains a user parameter, then a request to the API on the login url.

### upload.service, in which are implemented:
    - The uploadFile() method, which contains an fd parameter, then a request to the API on the uploaded / file url.
    - The updateFile() method, which contains an id parameter and an fd parameter, then a request to the API on the upload url.
    - The getMissingFiles() method, which contains an id_affair parameter, then an API request on the missing_files url.
    - The getUploadedFiles() method, which contains an id_affair parameter, then an API request on the uploadedd_files url.
    - The getReceivedFiles() method, which contains an id_affair parameter, then an API request on the received_files url.
    - The getFileByPath() method, which contains a path parameter, then a request to the API on the uploaded url.
    
The requests are made thanks to the HttpClient module of Angular.

## Models
The JLDropOff project contains the following model:
### User.model, in which the structure of User is defined.

## Pages
The JLDropOff project contains the following pages:
### home
    - home.page.html, this page contains the first view of the application (home),
    with a button to access the register page,
    and a button to access the login page.

### authentication (login / register)
    - login.page.ts, when the page is initialized, the form initialization method is called.
    The onSubmit() method is executed when the button is clicked (in the form),
    a call to the login method of the authenticationService is made, the parameter passed is the content of the form.

    - login.page.html, this page contains the login form, with the form validators and the onSubmit button.

    - registration.page.ts, when the page is initialized, the form initialization method is called.
    The onSubmit() method is executed when the button is clicked (in the form),
    a call to the register method of the authenticationService is made, the parameter passed is the content of the form.

    - register.page.html, this page contains the register form, with the form validators and the onSubmit button.

### affair
    - affair.page.ts, when the page is initialized, the user's belongings are collected,
    by the getAffair() method of the Affair service. User id (stored in storage),
    is passed as a parameter of the method. The timeout method is also called at initialization,
    indeed, when the user has been on this page for more than 15 minutes,
    the automatic disconnection is carried out and the storage is emptied. Then the page contains the onGetFiles () method,
    at the execution of this one, a redirection is done on the file-list page.

    - affair.page.html, this page contains the returned view, it contains a first block of current affairs,
    then a second completed business.

### files (file-details / file-list / upload-file)
    - file-details.page.ts, when the page is initialized, elements of the path are replaced,
    then the getFile method is called, with the path as a parameter.
    The takePicture method is called when the user takes a photo.
    If the user chooses a file in his directory, the handleFileInput method is called.
    The dataURLtoBlob method is used to convert a file into a blob.
    Finally, the onFormData method is used to convert the file or the blob into formData,
    then to set the attributes of the formData, before calling the updateFile method of the uploadService,
    which is used to update a file.

    - file-details.page.html, in a first block appears the image, with its type and date of creation,
    as well as a button to open the update items. If the user clicks, he can choose if he wants to send
    a file or take a photo, the action is executed when the Send button is clicked.

    - file-list.page.ts, when the page is initialized, the 3 file recovery methods are called.
    The onAddFile() method is executed when clicking on a file to upload, it redirects to the file upload page.
    The onDetails() method is used to redirect the user to the file details page.

    - file-list.page.html, the page contains 3 blocks which make it possible to differentiate whether the files are missing, uploaded or received.
    Then a 4th block which allows to visualize the file on which he clicked.

    - upload-file.page.ts, when the page is initialized, the form initialization method is called.
    The takePicture method is called when the user takes a photo.
    If the user chooses a file in his directory, the handleFileInput method is called.
    The dataURLtoBlob method is used to convert a file into a blob.
    Finally, the onFormData method is used to convert the file or the blob into formData,
    then to set the attributes of the formData, before calling the uploadFile method of the uploadService,
    which is used to upload a file.

    - upload-file.page.html, the page contains an upload form, in which he can choose a file from his directory
    or take a photo. The form is sent by clicking on the Send button.

In each page there is a method to return to the previous page: onReturn(),
as well as a method to log out: onLogout().