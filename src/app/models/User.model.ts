export class User{
    email: string;
    password: string;
    lastname: string;
    firstname: string;
    validated_profile: boolean;
}