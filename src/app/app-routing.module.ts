import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./authentication/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./authentication/register/register.module').then(m => m.RegisterPageModule)
  },
  {
    path: 'affair',
    loadChildren: () => import('./affair/affair.module').then( m => m.AffairPageModule)
  },
  {
    path: 'upload-file/:affair_id/:document_id',
    loadChildren: () => import('./files/upload-file/upload-file.module').then( m => m.UploadFilePageModule)
  },
  {
    path: 'file-list/:affair_id/:status',
    loadChildren: () => import('./files/file-list/file-list.module').then( m => m.FileListPageModule)
  },
  {
    path: 'file-details/:path/:bool/:status',
    loadChildren: () => import('./files/file-details/file-details.module').then( m => m.FileDetailsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
