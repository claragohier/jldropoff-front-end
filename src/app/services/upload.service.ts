import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private http: HttpClient) { }
  url: string = 'https://whispering-beyond-74155.herokuapp.com';

  uploadFile(fd: FormData) {
    return this.http.post(this.url + '/uploaded/file', fd);
  }

  updateFile(id: number, fd: FormData){
    return this.http.post(this.url + '/upload/' + id, fd);
  }

  getMissingFiles(id_affair: number) {
    return this.http.get(this.url + '/missing_files/' + id_affair);
  }

  getUploadedFiles(id_affair: number) {
    return this.http.get(this.url + '/uploaded_files/' + id_affair);
  }

  getReceivedFiles(id_affair: number) {
    return this.http.get(this.url + '/received_files/' + id_affair);
  }

  getFileByPath(path: string){
    return this.http.get(this.url + '/uploaded' + path);
  }
}
