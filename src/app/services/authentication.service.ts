import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../models/User.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }
  url: string = 'https://whispering-beyond-74155.herokuapp.com';

  registration(user: User) {
    return this.http.put(this.url + '/user_register', user);
  }

  login(user: User) {
    return this.http.post(this.url + '/login', user);
  }
}
