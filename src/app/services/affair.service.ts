import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AffairService {

  constructor(private http: HttpClient) { }
  url: string = 'https://whispering-beyond-74155.herokuapp.com';

  getAffair(user_id: number){
    return this.http.get(this.url + '/affair/' + user_id);
  }
}