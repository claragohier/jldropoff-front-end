import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Plugins, CameraResultType } from '@capacitor/core';

import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';

import { UploadService } from '../../services/upload.service';

const { Camera } = Plugins;

@Component({
  selector: 'app-file-details',
  templateUrl: './file-details.page.html',
  styleUrls: ['./file-details.page.scss'],
})
export class FileDetailsPage implements OnInit {

  constructor(private router: Router,
    private route: ActivatedRoute,
    private storage: Storage,
    private alertCtrl: AlertController,
    private uploadService: UploadService) { }

  ftp = 'http://stage.santeassurance.eu/clara';
  path = this.route.snapshot.params.path;
  condition = this.route.snapshot.params.bool;
  status = this.route.snapshot.params.status;
  newPath;
  file;
  label;
  date;
  affair_id;
  document_id;
  file_id;

  update: boolean = false;
  fileForm: FormGroup;
  contentForm: FormGroup;
  file_to_upload: File = null;
  load: boolean = false;
  pdf: boolean = false;
  image: boolean = false;


  // initialization of the page, redefinition of the file path, display of the file, display condition
  ngOnInit() {
    this.newPath = this.path.replace('%2F', '/');
    this.newPath = this.newPath.replace('%2F', '/');
    this.newPath = this.newPath.replace('%2F', '/');
    this.newPath = this.newPath.replace('%2F', '/');
    this.newPath = this.newPath.replace('%2F', '/');
    // console.log(this.newPath);
    this.file = this.newPath.replace(this.ftp, '');
    this.path = this.file.replace('.', '/');
    console.log(this.path);

    this.getFile(this.path);
    this.timeOut();

    var extension = this.file.split('.').pop();
    if (extension === 'pdf') {
      return this.pdf = true;
    } else if (extension != 'pdf') {
      return this.image = true;
    }
  }

  // on click, return to the file list page
  onReturn() {
    this.router.navigate(['/file-list/' + this.affair_id + '/' + this.status]);
  }

  // logout, emptying the storage
  onLogout() {
    this.router.navigate(['/home']);
    this.storage.remove('user');
  }

  // on click, display the update form
  onUpdate() {
    this.update = true;
    this.initForm();
  }

  // on click, call of the file conversion method
  onSubmit() {
    var file = this.file_to_upload;
    this.onFormData(file);
  }

  // file recovery
  handleFileInput(files: FileList) {
    this.file_to_upload = files.item(0);
  }

  // method for capturing photos, call of the blob method and the file conversion method
  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.DataUrl
    });

    var file = this.dataURLtoBlob(image.dataUrl);
    this.onFormData(file);
  }

  // conversion of data url to blob
  dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], { type: mime });
  }

  // file conversion method where the form data is defined, call of update method
  onFormData(file) {
    var fd = new FormData();

    fd.set('file', file);

    var get_file = fd.get('file');
    var name = get_file['name'];

    if(name === 'blob'){
      fd.set('file', file, 'file.png');
    }
    
    fd.set('content', JSON.stringify(this.contentForm.value));

    this.uploadService.updateFile(this.file_id, fd).subscribe(result => {
    }, err => {
      this.load = true;
      setInterval(() => {
        this.router.navigate(['file-list/' + this.affair_id + '/0']);
        setTimeout(() => { window.location.reload() }, 10);
      }, 2000);
    });
  }

  // file recovery to display
  private getFile(path) {
    this.uploadService.getFileByPath(path).subscribe(file => {
      this.file_id = file['id'];
      this.affair_id = file['fk_affair']['id'];
      this.document_id = file['fk_document']['id'];
      this.label = file['fk_document']['label'];
      this.date = new Date(file['created_date']).toLocaleDateString();
    });
  }

  // logout the user after 15 minutes on the same page
  async timeOut() {
    var newAlert = await this.alertCtrl.create({
      header: "Session expirée",
      message: "Vous avez été inactif pendant " + this.msToMinutes() + " minutes, veuillez vous reconnecter",
      buttons: ['OK']
    })

    window.setTimeout(() => {
      this.router.navigate(['/home']);
      this.storage.remove('user');
      newAlert.present();
    }, 900000);
  }

  // initialization of the forms, using validators
  private initForm() {
    let file: File = null;
    let upload = false;
    let affair_id = this.affair_id;
    let document_id = this.document_id;

    this.fileForm = new FormGroup({
      file: new FormControl(file, Validators.required),
    });

    this.contentForm = new FormGroup({
      upload: new FormControl(upload),
      fkAffair: new FormControl(affair_id),
      fkDocument: new FormControl(document_id)
    });
  }

  // converting milliseconds to minutes format
  private msToMinutes() {
    const milliseconds = 900000;

    // const hours = `0${new Date(milliseconds).getHours() - 1}`.slice(-2);
    const minutes = `0${new Date(milliseconds).getMinutes()}`.slice(-2);
    // const seconds = `0${new Date(milliseconds).getSeconds()}`.slice(-2);

    const time = `${minutes}`;
    return time;
  }
}
