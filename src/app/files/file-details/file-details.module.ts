import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import { IonicModule } from '@ionic/angular';

import { FileDetailsPageRoutingModule } from './file-details-routing.module';
import { FileDetailsPage } from './file-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    FileDetailsPageRoutingModule,
    PdfViewerModule
  ],
  declarations: [FileDetailsPage]
})
export class FileDetailsPageModule {}
