import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Plugins, CameraResultType } from '@capacitor/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';

import { UploadService } from '../../services/upload.service';

const { Camera } = Plugins;

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.page.html',
  styleUrls: ['./upload-file.page.scss'],
})
export class UploadFilePage implements OnInit {

  constructor(private uploadService: UploadService,
    private route: ActivatedRoute,
    private router: Router,
    private storage: Storage,
    private alertCtrl: AlertController) { }

  fileForm: FormGroup;
  contentForm: FormGroup;
  file_to_upload: File = null;
  affair_id = this.route.snapshot.params.affair_id;
  document_id = this.route.snapshot.params.document_id;

  // initialization of the page, form initialization method call and time out
  ngOnInit() {
    this.initForm();
    this.timeOut();
  }

  // on click, return to the file list page
  onReturn() {
    this.router.navigate(['file-list/' + this.affair_id + '/0']);
  }

  // logout, emptying the storage
  onLogout() {
    this.router.navigate(['/home']);
    this.storage.remove('user');
  }

  // on click, call of the file conversion method
  onSubmit() {
    var file = this.file_to_upload;
    this.onFormData(file);
  }

  // file recovery
  handleFileInput(files: FileList) {
    this.file_to_upload = files.item(0);
  }

  // method for capturing photos, call of the blob method and the file conversion method
  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.DataUrl
    });

    var file = this.dataURLtoBlob(image.dataUrl);
    this.onFormData(file);
  }

  // conversion of data url to blob
  dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], { type: mime });
  }

  // file conversion method where the form data is defined, call of upload method
  onFormData(file) {
    var fd = new FormData();

    fd.set('file', file);

    var get_file = fd.get('file');
    var name = get_file['name'];

    if(name === 'blob'){
      fd.set('file', file, 'file.png');
    }
    
    fd.set('content', JSON.stringify(this.contentForm.value));

    this.uploadService.uploadFile(fd).subscribe(result => {
      // console.log(result);
    }, err => {
      // console.log(err);
      setInterval(() => {
        this.router.navigate(['file-list/' + this.affair_id + '/0']);
        setTimeout(() => { window.location.reload() }, 1);
      }, 100);
    });
  }

  // logout the user after 15 minutes on the same page
  async timeOut() {
    var newAlert = await this.alertCtrl.create({
      header: "Session expirée",
      message: "Vous avez été inactif pendant " + this.msToMinutes() + " minutes, veuillez vous reconnecter",
      buttons: ['OK']
    })

    window.setTimeout(() => {
      this.router.navigate(['/home']);
      this.storage.remove('user');
      newAlert.present();
    }, 900000);
  }

  // initialization of the forms, using validators
  private initForm() {
    let file: File = null;
    let upload = false;
    let affair_id = this.affair_id;
    let document_id = this.document_id;

    this.fileForm = new FormGroup({
      file: new FormControl(file, Validators.required),
    });

    this.contentForm = new FormGroup({
      upload: new FormControl(upload),
      fkAffair: new FormControl(affair_id),
      fkDocument: new FormControl(document_id)
    });
  }

  // converting milliseconds to minutes format
  private msToMinutes() {
    const milliseconds = 900000;

    // const hours = `0${new Date(milliseconds).getHours() - 1}`.slice(-2);
    const minutes = `0${new Date(milliseconds).getMinutes()}`.slice(-2);
    // const seconds = `0${new Date(milliseconds).getSeconds()}`.slice(-2);

    const time = `${minutes}`;
    return time;
  }
}
