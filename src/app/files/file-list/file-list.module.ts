import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import { IonicModule } from '@ionic/angular';

import { FileListPageRoutingModule } from './file-list-routing.module';
import { FileListPage } from './file-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FileListPageRoutingModule,
    PdfViewerModule
  ],
  declarations: [FileListPage]
})
export class FileListPageModule {}
