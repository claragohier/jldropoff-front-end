import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';

import { UploadService } from '../../services/upload.service';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.page.html',
  styleUrls: ['./file-list.page.scss'],
})
export class FileListPage implements OnInit {

  constructor(private uploadService: UploadService,
    private router: Router,
    private route: ActivatedRoute,
    private storage: Storage,
    private alertCtrl: AlertController) { }

  files: any;
  uploadedFiles: any;
  receivedFiles: any;
  uploaded: boolean;
  image: boolean = false;
  pdf: boolean = false;
  received: boolean = false;

  affair_id: number = this.route.snapshot.params.affair_id;
  status: boolean = this.route.snapshot.params.status;
  ftp = 'http://stage.santeassurance.eu/clara';
  affair = '/Affaire_' + this.affair_id + '/';
  path: string;


  // initialization of the page, call of three recovery methods and time out
  ngOnInit() {
    this.getUploadedFiles();
    this.getMissingFiles();
    this.getReceivedFiles();
    this.timeOut();
  }

  // on click, return to the affairs page
  onReturn() {
    this.router.navigate(['/affair']);
  }

  // logout, emptying the storage
  onLogout() {
    this.router.navigate(['/home']);
    this.storage.remove('user');
    // console.log(this.storage.get('user'));
  }

  // on click, redirect to page for adding file
  onAddFile(id: string) {
    this.router.navigate(['upload-file/' + this.affair_id + '/' + id]);
  }

  // differentiation of document types
  onSavePath(path, type) {
    this.path = this.ftp + path;
    var extension = this.path.split('.').pop();

    if (type === "true") {
      this.received = true;
    } else {
      this.received = false;
    }

    if (extension === 'pdf') {
      this.image = false;
      this.pdf = true;
    } else if (extension != 'pdf') {
      this.pdf = false;
      this.image = true;
    }
  }

  // redirect for display details
  onDetails(path, type) {
    const search = '/';
    const replaceWith = '%2F';

    const newPath = path.split(search).join(replaceWith);
    // console.log(newPath);
    this.router.navigate(['file-details/' + newPath + '/' + type + '/' + this.status]);
  }


  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  // logout the user after 15 minutes on the same page
  async timeOut() {
    var newAlert = await this.alertCtrl.create({
      header: "Session expirée",
      message: "Vous avez été inactif pendant " + this.msToMinutes() + " minutes, veuillez vous reconnecter",
      buttons: ['OK']
    })

    window.setTimeout(() => {
      this.router.navigate(['/home']);
      this.storage.remove('user');
      newAlert.present();
    }, 900000);
  }

  // call a method of the service to recover the missing files
  private getMissingFiles() {
    this.uploadService.getMissingFiles(this.affair_id).subscribe(files => {
      this.files = files;
    });
  }

  // call a method of the service to recover the uploaded files
  private getUploadedFiles() {
    this.uploadService.getUploadedFiles(this.affair_id).subscribe(uploadedFiles => {
      this.uploadedFiles = uploadedFiles;
    });
  }

  // call a method of the service to recover the received files
  private getReceivedFiles() {
    this.uploadService.getReceivedFiles(this.affair_id).subscribe(receivedFiles => {
      this.receivedFiles = receivedFiles;
    });
  }

  // converting milliseconds to minutes format
  private msToMinutes() {
    const milliseconds = 900000;

    const hours = `0${new Date(milliseconds).getHours() - 1}`.slice(-2);
    const minutes = `0${new Date(milliseconds).getMinutes()}`.slice(-2);
    const seconds = `0${new Date(milliseconds).getSeconds()}`.slice(-2);

    const time = `${minutes}`;
    return time;
  }
}