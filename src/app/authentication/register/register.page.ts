import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  constructor(private authenticationService: AuthenticationService,
    private router: Router) { }

  registerForm: FormGroup;
  res: boolean = true;
  confirm: boolean = true;

  // page initialization, form initialization method call
  ngOnInit() {
    this.initForm();
  }

  // on click, return to the home page
  onReturn() {
    this.router.navigate(['/home']);
  }

  // when sending the form, call the register method of the service, redirect to login page
  onSubmit() {
    if (this.registerForm.valid) {
      // console.log(this.registerForm.value['password']);
      if (this.registerForm.value['password'] == this.registerForm.value['confirm_password']) {
        this.authenticationService.registration(this.registerForm.value).subscribe(res => {
        }, err => {
          var status = err['status'];
          if (status === 200) {
            this.res = true;
            this.router.navigate(['/login']);
            console.log(status);
          } else {
            return this.res = false;
          }
          this.confirm = true;
        });
      } else {
        return this.confirm = false;
      }
    }
  }

  // initialization of the form, using validators
  private initForm() {
    let email = "";
    let password = "";
    let confirm_password = "";
    let validated_profile = false;

    this.registerForm = new FormGroup({
      email: new FormControl(email, Validators.required),
      password: new FormControl(password, Validators.required),
      confirm_password: new FormControl(confirm_password),
      validated_profile: new FormControl(validated_profile)
    });
  }
}

