import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../services/authentication.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private authenticationService: AuthenticationService,
    private storage: Storage,
    private router: Router) { }

  loginForm: FormGroup;
  res: boolean = true;
  confirm: boolean = true;

  // page initialization, form initialization method call
  ngOnInit() {
    this.initForm();
  }

  // on click, return to the home page
  onReturn() {
    this.router.navigate(['/home']);
  }

  // when sending the form, call the login method of the service, redirect to affairs page
  onSubmit() {
    if (this.loginForm.valid) {
      this.authenticationService.login(this.loginForm.value).subscribe(result => {
        // console.log(result);
        this.storage.set('user', result);
        if (result['validated_profile'] != 0) {
          setInterval(() => {
            this.router.navigate(['affair']);
            setTimeout(() => { window.location.reload() }, 100);
          }, 1000);
        } else {
          console.log(result);
          this.confirm = false;
        }
      }, err => {
        console.log(err);
        this.res = false;
      });
    }
  }

  // initialization of the form, using validators
  private initForm() {
    let username = "";
    let password = "";

    this.loginForm = new FormGroup({
      username: new FormControl(username, Validators.required),
      password: new FormControl(password, Validators.required)
    });
  }
}
