import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AffairPage } from './affair.page';

describe('AffairPage', () => {
  let component: AffairPage;
  let fixture: ComponentFixture<AffairPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffairPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AffairPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
