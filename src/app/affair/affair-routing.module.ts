import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AffairPage } from './affair.page';

const routes: Routes = [
  {
    path: '',
    component: AffairPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AffairPageRoutingModule {}
