import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AffairPageRoutingModule } from './affair-routing.module';
import { AffairPage } from './affair.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AffairPageRoutingModule
  ],
  declarations: [AffairPage]
})
export class AffairPageModule {}
