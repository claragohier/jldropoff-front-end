import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';

import { AffairService } from '../services/affair.service';

@Component({
  selector: 'app-affair',
  templateUrl: './affair.page.html',
  styleUrls: ['./affair.page.scss'],
})
export class AffairPage implements OnInit {

  constructor(private affairService: AffairService,
    private storage: Storage,
    private router: Router,
    private alertCtrl: AlertController) { }

  affairs: any;
  elements0: any;
  elements1: any;

  // initialization of the page, used to display the user's affairs
  ngOnInit() {
    this.storage.get('user').then((user) => {
      this.affairService.getAffair(user.id).subscribe(affair => {
        this.affairs = affair;
        this.elements0 = this.affairs[0];
        this.elements1 = this.affairs[1].reverse();
      });
    });

    this.timeOut();
  }

  // logout, emptying the storage
  onLogout() {
    this.router.navigate(['/home']);
    this.storage.remove('user');
  }

  // on click, display document details
  onGetFiles(id: string, status: boolean) {
    this.router.navigate(['file-list/' + id + '/' + status]);
  }

  // logout the user after 15 minutes on the same page
  async timeOut() {
    var newAlert = await this.alertCtrl.create({
      header: "Session expirée",
      message: "Vous avez été inactif pendant " + this.msToMinutes() + " minutes, veuillez vous reconnecter",
      buttons: ['OK']
    })

    window.setTimeout(() => {
      this.router.navigate(['/home']);
      this.storage.remove('user');
      newAlert.present();
    }, 900000);
  }

  // converting milliseconds to minutes format
  private msToMinutes() {
    const milliseconds = 900000;

    const hours = `0${new Date(milliseconds).getHours() - 1}`.slice(-2);
    const minutes = `0${new Date(milliseconds).getMinutes()}`.slice(-2);
    const seconds = `0${new Date(milliseconds).getSeconds()}`.slice(-2);

    const time = `${minutes}`;
    return time;
  }
}
